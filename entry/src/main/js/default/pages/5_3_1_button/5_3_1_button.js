export default {
    data: {
        progress: 5,
        downloadText: "下载"
    },
    setProgress(e) {
        this.progress += 10;    //每次点击增加10%的进度
        this.downloadText = this.progress + "%";
        if (this.progress >= 100) {
            this.downloadText = "完成";   //到达100%时显示完成
        }
    }
}
