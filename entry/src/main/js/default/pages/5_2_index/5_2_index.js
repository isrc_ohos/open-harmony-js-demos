export default {
    data: {
        unFavoriteImage: "/common/unfavorite.png",
        isFavorite: false,
        number: 20,
        descriptionFirstParagraph:"The breakthrough of visual boundaries, the exploration of photography and videography, the liberation of power and speed, and the innovation of interaction are now ready to be discovered. Embrace the future with new possibilities.",
        descriptionSecondParagraph: "Lighting up infinite possibilities. The quad camera of HUAWEI is embraced by the halo ring. It is a perfect fusion of reflection and refraction. Still Mate, but a new icon.",
    },
    favorite() {
        var tempTotal;
        if (!this.isFavorite) {
            this.unFavoriteImage = "/common/favorite.png";
            tempTotal = this.number + 1;
        } else {
            this.unFavoriteImage = "/common/unfavorite.png";
            tempTotal = this.number -1;
        }
        this.number = tempTotal;
        this.isFavorite = !this.isFavorite;
    }

}

