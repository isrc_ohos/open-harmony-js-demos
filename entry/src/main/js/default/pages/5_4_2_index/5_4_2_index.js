/* index.js */
export default {
    data: {
        up: 'up',
        down: 'down',
    },
    focusUp: function() {
        this.up = 'up focused';
    },
    blurUp: function() {
        this.up = 'up';
    },
    keyUp: function() {
        this.up = 'up keyed';
    },
    focusDown: function() {
        this.down = 'down focused';
    },
    blurDown: function() {
        this.down = 'down';
    },
    keyDown: function() {
        this.down = 'down keyed';
    },
}
