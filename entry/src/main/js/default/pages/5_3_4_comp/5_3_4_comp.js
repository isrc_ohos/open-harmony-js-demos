/* 新建comp.js */
export default {
    props: {
        title: {
            default: 'title'
        },
    },
    data: {
        showword: false,
    },
    childClicked () {
        this.$emit('eventType1', {text: "收到子组件参数"});
        this.showword = !this.showword;
    },
}
