//second.js
import router from '@system.router'
export default {
    launch: function () {
        router.back();  //回到路由前的页面
    },
    launch2: function () {
        router.push({
            uri: 'pages/first/first',   //目标页面的路径
        })
    }
}
