//.js文件代码
export default {
    data: {
        Image: "/common/Phone_00.jpg", //最开始显示black视图
        Name: "black",
        tmp: 1,
    },
    Click() {
        if ( this.tmp == 0) {
            this.Image = "/common/Phone_00.jpg";   //第三次点击跳到第一视图
            this.Name = "black";
        }
        else if ( this.tmp == 1 ) {
            this.Image = "/common/Phone_01.jpg";    //第一次点击跳到第二视图
            this.Name = "orange";
        }
        else if ( this.tmp == 2 ) {
            this.Image = "/common/Phone_02.jpg";    //第二次点击跳到第三视图
            this.Name = "white";
        }
        this.tmp = ( this.tmp + 1) % 3 ;
    }
}

