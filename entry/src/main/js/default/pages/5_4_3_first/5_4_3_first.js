//first.js
import router from '@system.router';    //指定的页面。在调用router 方法之前，需要导入 router 模块
export default {
    launch: function () {
        router.push({
            uri: 'pages/second/second', //目标页面的路径
        })
    },
}
