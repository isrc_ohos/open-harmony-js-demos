# JsHelloWorld<a name="ZH-CN_TOPIC_0000001113383238"></a>

本示例基于OpenHarmony下的JavaScript UI框架，进行项目目录解读，JS FA、常用和自定义组件、用户交互、JS动画的实现，通过本示例可以基本了解和学习到JavaScript UI的开发模式。

本项目是基于OpenHarmony项目而不是HarmonyOS项目，请注意运行环境。
**请参考[OpenHarmony项目配置方法](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%E9%A1%B9%E7%9B%AE%E5%BC%80%E5%8F%91%E6%B5%81%E7%A8%8B.md)进行项目配置和运行。**

如果你不熟悉OpenHarmony的JS开发，**请参考该项目的[开发讲解](https://gitee.com/isrc_ohos/ultimate-harmony-reference/blob/master/OpenHarmony%20JS%20Demo%E5%BC%80%E5%8F%91%E8%AE%B2%E8%A7%A3.md)。**

## Demo介绍
* 项目名称：OpenHarmony-JS-Demos
* 所属系列：OpenHarmony下的的JS Demo开发示例
* 开发版本：OpenHarmony-SDK-2.0-Canary，DevEco Studio 2.1 Release
* Demo来源：《鸿蒙操作系统应用开发实践》
* 项目作者和维护人：陈美汝
* 邮箱：isrc_hm@iscas.ac.cn

## Demo目录
本Demo的Pages目录下，每一个page均为一个简单的示例，其命名“5_`*`_`*`_name”，均对应上述书中的章节号，这里对每一个page示例进行详细介绍：
* 5_2_index：开发一个JS FA
* 5_3_1_button：基础组件Button
* 5_3_1_menu：基础组件Meun
* 5_3_1_text：基础组件Text
* 5_3_2_list-item-group：List组件list-item-group
* 5_3_2_list：List组件list-item
* 5_3_3_tabs：组件Tabs
* 5_3_4_comp, 5_3_4_index：自定义组件
* 5_4_1_index：用户交互-手势事件
* 5_4_2_index：用户交互-按键事件
* 5_4_3_first, 5_4_3_second：页面路由
* 5_5_1_transform：transform静态动画
* 5_5_2_animation：animation连续动画

## 使用说明
1. 下载OpenHarmony-JS-Demos项目，启动 DevEco Studio并打开工程。

2. 进入entry->src->main->config.json，修改js->pages标签下的文件路径，将需要运行的page路径放在首位。如下图，运行后会执行5_3_1_button。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0723/111621_4b1e3a62_8496150.png "WX20210723-111527@2x.png")

## 版权和许可信息
OpenHarmony-JS-Demos经过Apache License, version 2.0授权许可。
